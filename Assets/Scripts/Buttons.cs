﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Buttons : MonoBehaviour{

    public Buttons buttons;

    void Start() {
        buttons = GetComponent<Buttons>();
    }

    public void ExitGame() {
        Application.Quit();
    }
}
