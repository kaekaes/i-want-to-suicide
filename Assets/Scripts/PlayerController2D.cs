﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Photon.Pun;

public class PlayerController2D : Photon.Pun.MonoBehaviourPun {

    //Values iniciales para comprobaciones
    bool player = false;
    Transform tr;
    Rigidbody2D rb;

    [Header("Movement&Camera")]
    public bool CanMoveKeyboard = true;
    //Movement control
    public float MoveSpeed = 10f;
    //jump
    [Range(1,10)]
    public float JumpForce = 6f;
    public bool Jumping = false;
    float SpeedFallMultiplier = 7f;
    float FallMultiplier = 3f;
    float LowJumpMultiplier = 2.0f;

    [Space]
    public bool OnGround = false;
    public LayerMask m_WhatIsGround;
    public Transform m_GroundCheck;
    public float k_GroundedRadius = .4f;
    public UnityEvent OnLandEvent;
    [System.Serializable]
    public class BoolEvent : UnityEvent<bool> { }


    void Awake(){
        player = true;
        tr = transform;
        rb = GetComponent<Rigidbody2D>();
        if (OnLandEvent == null)
            OnLandEvent = new UnityEvent();
    }

    void FixedUpdate(){
        if (!PhotonNetwork.OfflineMode) { //Si está online
            if (photonView.IsMine) {
                //Debug.Log("Online Mode");
                //Comprobar si está en el suelo
                IsOnGround();

                //control de dirección
                if (rb.velocity.x > 0) {
                    transform.rotation = new Quaternion(transform.rotation.x, 0, transform.rotation.z, 1);
                } else if (rb.velocity.x < 0) {
                    transform.rotation = new Quaternion(transform.rotation.x, -180, transform.rotation.z, 1);
                }

                //control de salto

                if (rb.velocity.y < 0) {
                    if (Input.GetAxis("Vertical") < 0) {
                        //Debug.Log("Caida: Case 1");
                        rb.velocity += Vector2.up * Physics2D.gravity * (SpeedFallMultiplier - 1) * Time.deltaTime;
					} else {
                        //Debug.Log("Caida: Case 2");
                        rb.velocity += Vector2.up * Physics2D.gravity * (FallMultiplier - 1) * Time.deltaTime;
                    }
                } else if (rb.velocity.y > 0 && !Input.GetButton("Jump")) {
                    if (Input.GetAxis("Vertical") < 0) {
                        //Debug.Log("Caida: Case 3");
                        rb.velocity += Vector2.up * Physics2D.gravity * (SpeedFallMultiplier - 1) * Time.deltaTime;
					} else {
                        //Debug.Log("Caida: Case 4");
                        rb.velocity += Vector2.up * Physics2D.gravity * (LowJumpMultiplier - 1) * Time.deltaTime;
                    }
                }

				if (CanMoveKeyboard) {
                    if (OnGround && Input.GetButtonDown("Jump")) {
                        rb.velocity = Vector2.up * JumpForce;
                        OnGround = false;
                    }
                    float Hmove = Input.GetAxis("Horizontal") * MoveSpeed * 100 * Time.deltaTime;
                    rb.velocity = new Vector2(Hmove, rb.velocity.y);
                    //Jumping = Input.GetAxis("Jump") != 0;
                }
            }
        } else { //Si no está online  
            //Debug.Log("Offline Mode");
            //Comprobar si está en el suelo
            IsOnGround();

            //control de dirección
            if (rb.velocity.x > 0) {
                transform.rotation = new Quaternion(transform.rotation.x, 0, transform.rotation.z, 1);
            } else if (rb.velocity.x < 0) {
                transform.rotation = new Quaternion(transform.rotation.x, -180, transform.rotation.z, 1);
            }

            //control de salto
            if (rb.velocity.y < 0) {
                if (Input.GetAxis("Vertical") < 0) {
                    //Debug.Log("Caida: Case 1");
                    rb.velocity += Vector2.up * Physics2D.gravity * (SpeedFallMultiplier - 1) * Time.deltaTime;
                } else {
                    //Debug.Log("Caida: Case 2");
                    rb.velocity += Vector2.up * Physics2D.gravity * (FallMultiplier - 1) * Time.deltaTime;
                }
            } else if (rb.velocity.y > 0 && !Input.GetButton("Jump")) {
                if (Input.GetAxis("Vertical") < 0) {
                    //Debug.Log("Caida: Case 3");
                    rb.velocity += Vector2.up * Physics2D.gravity * (SpeedFallMultiplier - 1) * Time.deltaTime;
                } else {
                    //Debug.Log("Caida: Case 4");
                    rb.velocity += Vector2.up * Physics2D.gravity * (LowJumpMultiplier - 1) * Time.deltaTime;
                }
            }


            if (CanMoveKeyboard) {
                if (OnGround && Input.GetButtonDown("Jump")) {
                    rb.velocity = Vector2.up * JumpForce;
                    OnGround = false;
                }
                float Hmove = Input.GetAxis("Horizontal") * MoveSpeed * 100 * Time.deltaTime;
                rb.velocity = new Vector2(Hmove, rb.velocity.y);

				StopAllCoroutines();
				StartCoroutine(TestForPlatforms());
			}
        }
    }
	IEnumerator TestForPlatforms() {
		Debug.Log("Testing");
		if (Input.GetAxis("Vertical") < 0) {
			PlatformEffector2D[] platforms = FindObjectsOfType<PlatformEffector2D>();
			foreach (PlatformEffector2D plat in platforms) {
				Debug.Log("Apagar");
				plat.gameObject.GetComponent<BoxCollider2D>().enabled = false;
				yield return new WaitForSeconds(0.3f);
			}
		} else
		if (Input.GetAxis("Vertical") >= 0) {
			PlatformEffector2D[] platforms = FindObjectsOfType<PlatformEffector2D>();
			foreach (PlatformEffector2D plat in platforms) {
				Debug.Log("Encendiendo");
				Debug.Log("Encender");
				plat.gameObject.GetComponent<BoxCollider2D>().enabled = true;
			}
		}
	}
    void IsOnGround() {
        bool wasGrounded = OnGround;
        OnGround = false;
        Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
        for (int i = 0; i < colliders.Length; i++) {
            if (colliders[i].gameObject != gameObject) {
                OnGround = true;
                if (!wasGrounded)
                    OnLandEvent.Invoke();
            }
        }
    }
}
