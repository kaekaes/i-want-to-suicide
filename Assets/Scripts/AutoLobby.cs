﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PhotonHashTable = ExitGames.Client.Photon.Hashtable;

namespace iwts.multiplayer {
    public class AutoLobby : MonoBehaviourPunCallbacks {

        // Variables del servidor
        PhotonHashTable Hashtables = new PhotonHashTable();


        //Botones de Connect y Join // Textos de log debajo de los botones
        public Button BtnConnect;
        public Button BtnRandomJoin;
        public Button BtnReady;
        public Text TxtLog;

        //Timer Sincronizado para todos los jugadores al alcanzar x personas en el gurpo.
        public Text TxtTimeForStart;
        public double TimeForStartIncremnetal;
        public double TimeForStartTime;
        public double TimeForStartTimer = 10f;
        public bool OnTimer = false;

        public Text TxtPlayerCount;
        public int playersCount;

        public byte maxPlayersPerRoom = 4;
        public byte minPlayersPerRoom = 2;
        public bool MinPlayersReachedMSG = false;

        public bool playerCreated = false;
        public GameObject player3D;
        public GameObject player2D;

        public void Connect() {
            if (!PhotonNetwork.IsConnected) {
                if (PhotonNetwork.ConnectUsingSettings()) {
                    TxtLog.text += "\nConnected to server";
                } else {
                    TxtLog.text += "\nFailed on connecting to server";
                }
            }
            OnTimer = false;
            MinPlayersReachedMSG = true;

        }

        public override void OnConnectedToMaster() {
            BtnConnect.interactable = false;
            BtnRandomJoin.interactable = true;
        }

        public void JoinRandom() {
            if (!PhotonNetwork.JoinRandomRoom()) {
                TxtLog.text += "\nFailed on joining to a room";
            }
        }

        public override void OnJoinRandomFailed(short returnCode, string message) {
            TxtLog.text += "\nThere is no room to join, creating one...";
            if (PhotonNetwork.CreateRoom(null, new Photon.Realtime.RoomOptions() { MaxPlayers = maxPlayersPerRoom })) {
                TxtLog.text += "\nRoom created";
                Hashtables.Add("AllPlayersReady", (int)0);
                PhotonNetwork.CurrentRoom.SetCustomProperties(Hashtables);
            } else {
                TxtLog.text += "\nFailed on creating a room";
            }
        }

        public override void OnJoinedRoom() {
            TxtLog.text += "\nJoined to a room";
            BtnRandomJoin.interactable = false;
            CreatePlayer3D();
            BtnReady.interactable = true;
        }

        void Update() {
            if (PhotonNetwork.CurrentRoom != null) {
                playersCount = PhotonNetwork.CurrentRoom.PlayerCount;
                TxtPlayerCount.text = playersCount + "/" + maxPlayersPerRoom;
            } else {
                TxtPlayerCount.text = "";
                TxtTimeForStart.text = "";
            }
            if (playersCount >= minPlayersPerRoom && !OnTimer && AllPlayersReady()) {
                if (PhotonNetwork.LocalPlayer.IsMasterClient) {
                    TimeForStartTime = PhotonNetwork.Time;
                    Hashtables.Add("TimeForStartTime", TimeForStartTime);
                    PhotonNetwork.CurrentRoom.SetCustomProperties(Hashtables);
                } else {
                    TimeForStartTime = double.Parse(PhotonNetwork.CurrentRoom.CustomProperties["TimeForStartTime"].ToString());
                }
                OnTimer = true;
            }

            if (OnTimer) {
                if (MinPlayersReachedMSG) {
                    TxtLog.text += "\nMinimum players reached, wait" + TimeForStartTimer + " seconds to start the game";
                    MinPlayersReachedMSG = false;
                }

                TimeForStartIncremnetal = PhotonNetwork.Time - TimeForStartTime;
                TxtTimeForStart.text = (TimeForStartTimer - (PhotonNetwork.Time - TimeForStartTime)).ToString("0");

                if (TimeForStartIncremnetal >= TimeForStartTimer) {
                    OnTimer = false;
                    PhotonNetwork.AutomaticallySyncScene = true;
                    PhotonNetwork.LoadLevel(1);
                }
            }
        }

        void CreatePlayer3D() {
            playerCreated = true;
            PhotonNetwork.Instantiate(player3D.name,new Vector3(0,1,0),Quaternion.identity);
        }

        public void ImReady() {
            BtnReady.interactable = false;
            int i = int.Parse(PhotonNetwork.CurrentRoom.CustomProperties["AllPlayersReady"].ToString());
            i++;
            Hashtables.Add("AllPlayersReady", i);
            PhotonNetwork.CurrentRoom.SetCustomProperties(Hashtables);
        }

        bool AllPlayersReady() {
            /* De momento no funciona, buscar info
            int i = int.Parse(PhotonNetwork.CurrentRoom.CustomProperties["AllPlayersReady"].ToString());
            if (i == playersCount) {
                return true;
            } else {
                return false;
            }*/
            return true;
        }
    }
}