﻿using UnityEngine;
using Photon.Pun;

public class PlayerController3D : MonoBehaviourPun{

    Transform tr;
    Rigidbody rb;
    float Multiplier;


    [Header("Movement&Camera")]
    public bool CanMoveKeyboard = true;
    public bool CanMoveMouse = true;
    //Movement control
    public float MoveSpeed = 1000f;
    //jump
    public bool OnGround = false;
    public bool Jumping = false;
    public float jumpForce = 6000f;
    //Camera control
    public Camera cam;
    public float MouseSensivility = 3f;
    public float WheelSensivility = 5f;

    void Start() {
        Multiplier = 1000f;
        tr = transform;
        rb = GetComponent<Rigidbody>();
        cam = Camera.main;
    }
    void FixedUpdate() {
        if (photonView.IsMine) {
            //Debug.Log(gameObject.name);
            KeyboardControl();
            MouseControl();
        } else {
            GetComponentInChildren<Camera>().enabled = false;
            return;
        }
    }

    private void KeyboardControl() {

        RaycastHit hit;
        OnGround = Physics.Raycast(tr.position, -tr.up, out hit, 1.1f);

        //Movimiento de teclado
        if (CanMoveKeyboard) {
            
            Vector3 sp = rb.velocity;

            float Vmove = Input.GetAxis("Vertical") * MoveSpeed * Multiplier * Time.deltaTime;
            float Hmove = Input.GetAxis("Horizontal") * MoveSpeed * Multiplier * Time.deltaTime;
            Jumping = Input.GetAxis("Jump") != 0;

            if (OnGround) {
                if (Jumping) {
                    rb.AddForce(tr.up * jumpForce * Multiplier);
                }
            }

            Vector3 side = Hmove * tr.right;
            Vector3 forward = Vmove * tr.forward;
            Vector3 endSpeed = side + forward;

            endSpeed.y = sp.y;

            rb.velocity = endSpeed;
        }

    }
    private void MouseControl() {
        //Movimiento de raton
        if (CanMoveMouse) {
            float Hmove = Input.GetAxis("Mouse X") * MouseSensivility;
            transform.Rotate(new Vector3(0, Hmove, 0));
        }

        float zOffSet = Input.GetAxis("Mouse ScrollWheel");
        Camera.main.transform.Translate(0, 0, zOffSet * WheelSensivility);

    }
}
